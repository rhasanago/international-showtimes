const { BaseHtmlParser, DefaultContext, DefaultRequestMaker, ValueGrabber, JsonFileWriter } = require('crawl-e')

let context = new DefaultContext()
let requestMaker = new DefaultRequestMaker()

class ItunesResponseParser extends BaseHtmlParser {
    constructor() {
	super()
	this.postTitleGrabber = new ValueGrabber('a @title')
	this.postUrlGraber = new ValueGrabber('a @href')
    }
    parsePostsList(response, context, callback) {
	let { container, parsingContext } = this.prepareHtmlParsing(response.text, context)
	this.parseList(
	    container,
	    parsingContext,
	    'posts',
	    { box: 'div.grid3-column > ul > li' },
	    (box, context, cb) => {
		cb(null, this.parsePostBox(box, context))
	    },
	    callback
	)}
    parsePostBox (box, context) {
	return {
	    title: this.postTitleGrabber.grabFirst(box, context),
	    src: this.postUrlGraber.grabFirst(box, context),
	}
    }
}

let outputWriter = new JsonFileWriter()
let responseParser = new ItunesResponseParser()

requestMaker.get('https://itunes.apple.com/at/genre/filme/id33', context, (err, res) => {
    if (err) {
	console.error(`Error: ` + res.status)
    }
    responseParser.parsePostsList(res, context, (err, posts) => {
	outputWriter.saveFile(posts, context, (err) => {
	    if (err) {
		console.error('Error on file writing:', err)
		return
	    }
	    console.log('File created, check output directory...', '👏')
	})
    })
})
